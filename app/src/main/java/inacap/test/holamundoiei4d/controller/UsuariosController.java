package inacap.test.holamundoiei4d.controller;

import android.content.ContentValues;
import android.content.Context;

import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.modelo.sqlite.UsuariosModel;

/**
 * Created by Nicolas stevens on 01-09-2017.
 */

public class UsuariosController {

    private UsuariosModel usuariosmodel;


    public UsuariosController(Context context) {
        this.usuariosmodel = new UsuariosModel(context);
    }

    public void crearUsuario(String username, String password1, String password2) throws Exception {


        if (password1.length() < 4) {
            throw new Exception("Su contrasseña debe tener min 4 caracteres");
        }
        if (!password1.equals(password2)) {
            //si las contraseñas  no coinsiden lanzamos un  exception
            //este debe ser manejado por la activity
            throw new Exception("Contraseñas no coinciden");
        }

        //llamar UsuarioModel para agregar un usuario a la base de datos
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, password1);

        this.usuariosmodel.crearUsuario(usuario);
    }
}
