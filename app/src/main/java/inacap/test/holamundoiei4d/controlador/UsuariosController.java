package inacap.test.holamundoiei4d.controlador;

import android.content.ContentValues;
import android.content.Context;

import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 01-09-17.
 */

public class UsuariosController {

    private UsuariosModel usuariosModel;

    public UsuariosController(Context context){
        this.usuariosModel = new UsuariosModel(context);
    }

    public void crearUsuario(String username, String password1, String password2) throws Exception{

        if(!password1.equals(password2)){
            // Si las contraseñas no coinciden lanzamos un Exception
            // Este debe ser manejado por la Activity
            throw new Exception("Contraseñas no coinciden");
        }

        // Llamar UsuarioModel para agregar un usuario a la base de datos
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_USERNAME, username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD, password1);

        this.usuariosModel.crearUsuario(usuario);
    }

    public boolean usuarioLogin(String username, String password) throws Exception{

        try{
            ContentValues usuario = this.usuariosModel.obtenerUsuarioPorUsername(username);
            if(password.equals(usuario.get(HolaMundoDBContract.HolaMundoUsuarios.COLUMN_NAME_PASSWORD))){
                return true;
            }
        } catch (Exception e){
            switch(e.getMessage()){
                case "Username no encontrado.":
                    throw new Exception(e.getMessage());
            }
            return false;
        }

        return false;
    }
}










