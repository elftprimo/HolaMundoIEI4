package inacap.test.holamundoiei4d.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoiei4d.MainActivity;
import inacap.test.holamundoiei4d.R;
import inacap.test.holamundoiei4d.modelo.sqlite.HolaMundoDBContract;
import inacap.test.holamundoiei4d.vista.fragmentos.BienvenidaFragment;
import inacap.test.holamundoiei4d.vista.fragmentos.SegundoFragment;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BienvenidaFragment.OnFragmentInteractionListener, SegundoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        //llamamos a la barra de herramientas

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //llamamos al boton flotante en la parte inferior de la pantalla

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mensaje temporal numero2
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //componente que permite la apertura del panel lateral

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //llamamos al panel lateral

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //escuchamos cuando la persona seleccione un item del panel lateral

        navigationView.setNavigationItemSelectedListener(this);

        //llamar la cabecera
        View cabecera =navigationView.getHeaderView(0);

        TextView tvUsername =(TextView) cabecera.findViewById(R.id.tvUsername);

        //tomamos el nombre usuario desde las preferencias
        SharedPreferences sesion = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME,Context.MODE_PRIVATE);
        String username = sesion.getString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME , "");

        //mostrar dato
        tvUsername.setText("Bienvenido " + username);

        navigationView.setCheckedItem(R.id.nav_camera);
        navigationView.getMenu().performIdentifierAction(R.id.nav_camera,0);
    }

    @Override
    public void onBackPressed() {

        //manejamos el comportamiento del boton "atras"

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        //consultamos si el panel lateral esta abierto

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            //si es asi, lo cerramos
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //de lo contrario, actuamos de manera normal
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //manejamos el comportamiento del menu de la barra de herramientas

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //tomamos el id
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else
            if(id == R.id.action_logout){
                SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERECENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sesiones.edit();

                editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);

                editor.commit();

                Intent v = new Intent(HomeDrawerActivity.this, MainActivity.class);
                startActivity(v);

                finish();
            }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //reaccionamos a la accion del usurio
        //debemos buscar la id del elemento seleccionado

        // Handle navigation view item clicks here.

        int id = item.getItemId();

        //creamos un fragmento nulo

        Fragment fragment = null;

        if (id == R.id.nav_camera) {
            fragment = new BienvenidaFragment();
        } else if (id == R.id.nav_gallery) {
            fragment = new SegundoFragment();
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }else if (id == R.id.nav_hola){
            Toast.makeText(getApplicationContext(),"hola",Toast.LENGTH_SHORT).show();

        }
        if (fragment!=null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
